#!/usr/bin/env python
"""
An example of a step using MLflow and Weights & Biases
"""
import pandas as pd
import argparse
import logging
import wandb


logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()


def go(args):

    run = wandb.init(job_type="step to clean data")
    run.config.update(args)

    artifact_local_path = run.use_artifact(args.input_artifact).file()
    df = pd.read_csv(artifact_local_path)
    logger.info("Downloaded artifact and loaded into dataframe")

    idx = df['price'].between(args.min_price, args.max_price) & df['latitude'].between(40.5, 41.2)
    df = df[idx].copy()
    logger.info("Filtered Dataframe with Min-Max price")
    df['last_review'] = pd.to_datetime(df['last_review'])
    df.to_csv("clean_sample.csv", index=False)

    artifact = wandb.Artifact(
        args.output_artifact,
        type= args.output_type,
        description=args.output_description)

    artifact.add_file("clean_sample.csv")
    run.log_artifact(artifact)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="step using mlflow and wandb")

    parser.add_argument(
        "--input_artifact",
        type=str,
        help="input artifact obtained from W&B",
        required=True
    )

    parser.add_argument(
        "--output_artifact",
        type=str,
        help="output name for cleaned data artifact provided as input",
        required=True
    )

    parser.add_argument(
        "--min_price",
        type=float,
        help="minimum price to be considered",
        required= True
    )

    parser.add_argument(
        "--max_price",
        type=float,
        help="maximum price to be considered",
        required=True
    )

    parser.add_argument(
        "--output_type",
        type=str,
        help="type of the artifact",
        required=True
    )

    parser.add_argument(
        "--output_description",
        type=str,
        help="artifact description",
        required=True
    )

    args = parser.parse_args()

    go(args)
